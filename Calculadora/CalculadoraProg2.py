

#Calculadora para Programación 2
#Alumno: Matías Molina.-


#Definimos un título 

print ("\n\n************ Menú Calculadora UAI ************\n\n")
print ("\t\t- OPCIONES- \n\n")

#Defino un menú
print ("ingrese un número de la operación que desea realizar")

print ("\tOperaciones: ")

operacion = 0
menu = False

#Mostramos menú
while (not menu):
   
    print (" 1- Sumar")
    print (" 2- Restar")
    print (" 3- Dividir")
    print (" 4- Multiplicar")
    print (" 5- Potencia")
    print (" 0- Salir\n")

    #tomamos el valor de la operacion 
    operacion = int(input("Ingrese el número de la operación: "))



     #si el valor de operacion es 0 salimos del while
    if operacion == 0:
        menu = True

    #definimos los valores a operar   
    primerValor = int(input("Ingrese el primer valor: "))
    segundoValor = int(input("Ingrese el segundo valor: "))

    # funciones para operaciones
    def Sumar(primerValor, segundoValor):
        print("El resultado de la suma es: ",primerValor+segundoValor)

    def Restar(primerValor, segundoValor):
        print("El resultado de la resta es: ",primerValor-segundoValor)

    def Dividir(primerValor, segundoValor):
        print("El resultado de la division es: ",primerValor/segundoValor)

    def Multiplicar(primerValor, segundoValor):
        print("El resultado de la multiplicacion es: ",primerValor*segundoValor)

    def Potencia(primerValor, segundoValor):
        print("El resultado de la Potencia es: ",primerValor**segundoValor)


    #Debería ser un switch Case pero no me salio :P 
    #llamadas a Operaciones con IF's: 
    if operacion == 1:
        Sumar(primerValor, segundoValor)
        print ("\n**** Operación terminada ****\n")
       
    elif operacion == 2:
        Restar(primerValor, segundoValor)
        print ("\n**** Operación terminada ****\n")

    elif operacion == 3:
        Dividir(primerValor, segundoValor)
        print ("\n**** Operación terminada ****\n")
    
    elif operacion == 4:
        Multiplicar(primerValor, segundoValor)
        print ("\n**** Operación terminada ****\n")

    elif operacion == 5:
        Potencia(primerValor, segundoValor)   
        print ("\n**** Operación terminada ****\n")     

    else:
        print ("Introducir una opción válida") 

  