
#Clase Impresora 
class Impresora():
    pass
    def __init__(self): #Constructor
        self.CartuchoNegro = 100
    
    def __del__(self): #Destructor
        print("Impresora Eliminada")
            
    @classmethod #Metodo de impresora B&N
    def Imprimir(self,CartuchoNegro,Copias):
        CartuchoNegro = CartuchoNegro - Copias
        str(CartuchoNegro)
        print("\n\nImprmiendo Blanco y Negro")
        return print("\nSe imprimió en Blanco y negro, quedando un porcentaje de: ",CartuchoNegro,"%")


#Clase Impresora Color
class ImpresoraColor(Impresora):
    def __init__(self):
        self.CartuchoCyan = 100
        self.CartuchoMagenta = 100
        self.CartuchoAmarillo = 100

    def __del__(self): #Destructor
        print("Impresora Color Eliminada")  
       
   
    @classmethod #Metodo de impresora Color
    def Imprimir(self, CartuchoAmarillo,CartuchoCyan,CartuchoMagenta,Copias):
        CartuchoAmarillo = CartuchoAmarillo - Copias
        CartuchoCyan = CartuchoCyan - Copias
        CartuchoMagenta = CartuchoMagenta - Copias
        str(CartuchoAmarillo)
        str(CartuchoCyan)
        str(CartuchoMagenta)
        return print("\nImprmiendo Color\n\nSe imprimió en Color Amarillo, quedando un porcentaje de: ",CartuchoAmarillo,"%","\nSe imprimió en Color Cyan, quedando un porcentaje de: ",CartuchoCyan,"%","\nSe imprimió en Color Magenta, quedando un porcentaje de: ",CartuchoMagenta,"%")
        

impByN = Impresora()
impByN.Imprimir(100,2)
print("\nimprimiendo copias en Blanco y negro: 2")

impColor = ImpresoraColor()
impColor.Imprimir(100,100,100,3)
print("\nimprimiendo copias en color: 3")

del impByN
del impColor